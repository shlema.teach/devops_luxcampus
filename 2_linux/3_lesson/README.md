1. Из командной строки попробуйте создать/удалить/изменить/скопировать/переместить файл используя на выбор любой эдитор

>touch test.txt
> 
>nano test.txt
> 
>cp test.txt test1.txt
> 
>mv test1.txt test2.txt
>
>rm test2.txt


2. добавть текст в файл с помощью перенаправления потока и использования <<<STOP ограничителя мультистрочного ввода

> guru@server1# touch lux.txt
> 
> guru@server1# cat <<STOP >lux.txt
> 
> heredoc> test1
> 
>heredoc> test1
>
>heredoc> test2 
> 
> heredoc> test3 
> 
>heredoc> STOP
>
>guru@server1# cat lux.txt
>test1
>
>test1
>
>test2
> 
>test3


3. вывести текст на екран, применить фильтры используя патерны и grep
4. с помощью sed заменить содержания вывода в консоль а потом и содержания файла
5. с помощью awk попробовать вывести разные параметры с разными разделителями как из файла так и из вывода любой команды типа ls