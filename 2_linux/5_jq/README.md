[How to work](https://stedolan.github.io/jq/tutorial/)

> > echo '{"fruit":{"name":"apple","color":"green","price":1.20}}' | jq '.'

![img](img/1.png)

Create fruit.json and add there next text: {"fruit":{"name":"apple","color":"green","price":1.20}}
> nano fruit.json
> 
> jq '.' fruit.json

![img](img/2.png)

![img](img/3.png)