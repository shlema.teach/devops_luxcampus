HOME TASK
 - [Find out the difference between Soft Link and Hard Link](https://ravesli.com/hard-links-and-symlinks-in-linux/)

 - how to customize shell promt
 - How a file access mask works (777, 655, etc)
 - Special Variable Shells ($!, $?, $@, etc)



 1) Perform the following sequence of operations:
- create a subdirectory test in the home directory;
- copy the .bash_history file to this directory while changing its name to labwork2;
- create a hard and soft link to the labwork2 file in the test subdirectory;
- how to define soft and hard link, what do these
concepts;
- change the data by opening a symbolic link. What changes will happen and why
- rename the hard link file to hard_lnk_labwork2;
- rename the soft link file to symb_lnk_labwork2 file;
- then delete the labwork2. What changes have occurred and why?

>mkdir Dir1
>
>cd Dir1/
>
>cp ~/.bash_history labwork2
> 
>ls -l
>
>ln labwork2 hard_labwork2
>
>ln -s labwork2 soft_labwork2
>
>ls -l
>
>cat soft_labwork2
>
>ls -l
>
>mv hard_labwork2 hard_lnk_labwork2
>
>mv soft_labwork2 symb_lnk_labwork2
>rm labwork2
>
>ls -l


![img.png](img/link.gif)