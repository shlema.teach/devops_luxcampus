#!/bin/bash

a=(12 1 13 14 73 2 39 11 22)
b=(1 32 22 87 12 19 111 23 44)

# Output the numbers in the column that are not contained in these arrays
#echo ${a[@]} ${b[@]} | tr ' ' '\n' | sort | uniq -u
#echo ----------------------------------------------

# Display array of common elements
set=$(echo ${a[@]} ${b[@]} | sed 's/ /\n/g' | sort | uniq -d)
echo $set
